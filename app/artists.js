const express = require('express');
const multer = require('multer');
const path = require('path');
const Artist = require('../models/Artist');
const nanoid = require('nanoid');
const router = express.Router();

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    // Artist get
    router.get('/', (req, res) => {
        Artist.find()
            .then(artists => res.send(artists))
            .catch(() => res.sendStatus(500))
    });

    // Artist post
    router.post('/', upload.single('image'), (req, res) => {
        const artistData = req.body;

        if (req.file) {
            artistData.image = req.file.filename;
        } else {
            artistData.image = null;
        }

        const artist = new Artist(artistData);

        artist.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;
